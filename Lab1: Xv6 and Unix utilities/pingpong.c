#include "kernel/types.h"
#include "kernel/stat.h"
#include "kernel/fcntl.h"
#include "user/user.h"
#include "kernel/param.h"

int main(int argc, char *argv[])
{
	int p1[2], p2[2];
	pipe(p1);//子进程->父进程
	pipe(p2);//父进程->子进程
	if (argc != 2) {
		printf("Parameter missing\n");
		exit(0);
	}

	char *buf = argv[1];

	int pid = fork();

	if (pid > 0) {
		close(p1[1]);
		close(p2[0]);

		if (write(p2[1], &buf, sizeof(char)) != sizeof(char)) {
			printf(" father write error \n");
			exit(0);
		}
		if (read(p1[0], &buf, sizeof(char)) != sizeof(char)) {
			printf(" father read error \n");
			exit(0);
		} else {
			printf("%d: received pong \n", getpid());
			exit(0);
		}
		close(p1[0]);
		close(p2[1]);
	} else if (pid == 0) {
		close(p1[0]);
		close(p2[1]);

		if (read(p2[0], &buf, sizeof(char)) != sizeof(char)) {
			printf(" child read error \n");
			exit(0);
		}
		if (write(p1[1], &buf, sizeof(char)) != sizeof(char)) {
			printf(" child write error \n");
			exit(0);
		} else {
			printf("%d: received ping \n", getpid());
			exit(0);
		}
		close(p1[1]);
		close(p2[0]);
	} else {
		printf(" fork error!\n ");
		close(p1[0]);
		close(p1[1]);
		close(p2[0]);
		close(p2[1]);
		exit(0);
	}
}

